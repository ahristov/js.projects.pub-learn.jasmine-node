describe("PUT record to CouchDB", function(){

	it("should get back same ID", function(){

		var request = require('request');
		// var qs = require('querystring');
		var rand = Math.floor(Math.random()*100000000).toString();
		var _ = require('underscore');

		request(
			{ method: 'PUT'
				, uri: 'http://mikeal.iriscouch.com/testjs/' + rand
				, multipart:
				[ { 'content-type': 'application/json'
					,  body: JSON.stringify({foo: 'bar', _attachments: {'message.txt': {follows: true, length: 18, 'content_type': 'text/plain' }}})
				}
					, { body: 'I am an attachment' }
				]
			}
			, function (error, response, body) {


				if(response.statusCode == 201){

					// console.log('document saved as: http://mikeal.iriscouch.com/testjs/'+ rand);
					// process.stdout.write('document saved as: http://mikeal.iriscouch.com/testjs/'+ rand);
					// new jasmine.Reporter().log('document saved as: http://mikeal.iriscouch.com/testjs/'+ rand);
					jasmine.getGlobal().console.log('document saved as: http://mikeal.iriscouch.com/testjs/'+ rand);

					var responseJSON = JSON.parse(body);

					expect(responseJSON.ok).toEqual(true);
					expect(responseJSON.id).toEqual(rand);
					expect(_.has(responseJSON, "rev")).toEqual(true);


					asyncSpecDone();

				} else {

					console.log('error: '+ response.statusCode);
					console.log(body);

				}
			}
		);


		asyncSpecWait();
	});
});

