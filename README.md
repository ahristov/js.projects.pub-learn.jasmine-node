# Jasmine

"**Jasmine** is a behavior-driven development framework
for testing your JavaScript code.
It does not depend on any other JavaScript frameworks.
It does not require a DOM. And it has a clean,
obvious syntax so that you can easily write tests."

> URL: https://github.com/mhevery/jasmine-node


## Installation

You can install the `jasmine-node` and `request` using `npm`:

	npm install jasmine-node -g
	npm install request -g

you can run then:

	jasmine-node

Note: See next section on how to add node
modules dependencies in your project
so that you can distribute them together
with your code and no need to
install on the client mashine.


## Setup dependencies in project's git repository

From the project's home directory add
all the node modules the project depends on:

	git submodule add git://github.com/mhevery/jasmine-node.git node_modules/jasmine
	git submodule add git://github.com/larrymyers/jasmine-reporters.git node_modules/jasmine-reporters
	git submodule add git://github.com/substack/node-findit.git node_modules/findit
	git submodule add git://github.com/substack/node-seq.git node_modules/seq
	git submodule add git://github.com/substack/node-hashish.git node_modules/hashish
	git submodule add git://github.com/substack/js-traverse.git node_modules/traverse
	git submodule add https://github.com/substack/node-chainsaw.git node_modules/chainsaw

	git submodule add git://github.com/documentcloud/underscore.git node_modules/underscore
	git submodule add git://github.com/maritz/node-sprintf.git node_modules/sprintf
	git submodule add git://github.com/mikeal/request.git node_modules/request
	git submodule add git://github.com/jazzychad/querystring.node.js.git node_modules/querystring

Now, when cloning the repository, nodeunit can be downloaded by doing the following:

	git submodule init
	git submodule update


Note: To remove a submodule from git repository:

- Delete the relevant line from the .gitmodules file.
- Delete the relevant section from .git/config.
- Run git rm --cached path_to_submodule (no trailing slash).

Example:

	git rm --cached node_modules/shred

- Commit to git
- Delete the now untracked submodule files.



You can now run `jasmine-node` from the project's home directory:

	node node_modules\jasmine\bin\jasmine-node

	USAGE: jasmine-node [--color|--noColor] [--verbose] [--coffee] directory

	Options:
	  --autotest         - rerun automatically the specs when a file changes
	  --color            - use color coding for output
	  --noColor          - do not use color coding for output
	  -m, --match REGEXP - load only specs containing "REGEXPspec"
	  --verbose          - print extra information per each test run
	  --coffee           - load coffee-script which allows execution .coffee files
	  --junitreport      - export tests results as junitreport xml format
	  --teamcity         - converts all console output to teamcity custom test runner commands. (Normally auto detected.)
	  --runWithRequireJs - loads all specs using requirejs instead of node's native require method
	  --test-dir         - the absolute root directory path where tests are located
	  --nohelpers        - does not load helpers.
	  -h, --help         - display this help and exit


-or-

you can also use:

	node node_modules\jasmine\lib\jasmine-node\cli.js



## Project structure

Let's follow this convention:

	README.md			this file
	node_modules		contains node modules relevant for the project
	reports				the jUnit compatable reports after running the tests
	run-tests.bat		use from Hudson to run the tests
	spec				contains the unit test specifications

Please note the spec files for Jasmine should be named like '*.spec.js'



## Running the tests

Running the tests from command prompt:

	$ node node_modules\jasmine\bin\jasmine-node spec
	.

	Finished in 1.717 seconds
	1 test, 3 assertions, 0 failures



## Links

[jasmine-node](https://github.com/mhevery/jasmine-node "link to jasmine-node")

[jasmine](https://github.com/pivotal/jasmine/wiki "link to jasmine")

[Blog post](http://www.salientblue.com/blog/?p=216)





